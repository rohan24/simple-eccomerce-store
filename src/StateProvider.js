// setup data layer/ React context API
// prevent propdrilling , grandparent-parent-children

import React, {createContext, useContext, useReducer } from "react";

// Data Layer
export const StateContext = createContext();

//Build a provider
export const StateProvider = ({ reducer, initialState, children}) => (
   <StateContext.Provider value={useReducer(reducer, initialState)}>
        {children}
   </StateContext.Provider>
);

// using inside of a component
export const useStateValue = () => useContext(StateContext);


