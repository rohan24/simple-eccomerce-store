import React, { useState } from 'react';
import './Login.css';
import { Link, useHistory } from "react-router-dom";
import { auth } from "./firebase";
import logo from './images/mylogo.PNG';


function Login() {
    const history = useHistory();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const login = (event) => {
        event.preventDefault();

        // do login logic
        
        auth.signInWithEmailAndPassword(email,password)
            .then((auth) => {
                //logged in
                history.push('/');
            })
            .catch((e) => alert(e.message));
    };

    const register = (event) => {
        event.preventDefault();

        // do register logic

        auth.createUserWithEmailAndPassword(email,password)
        .then((auth) => {
            //created user
            history.push('/');
        })
        .catch((e) => alert(e.message));
    };
    return (
        <div className="login">

            <Link to="/">
                <img className="login__image" src={logo} alt="login-image"/>
            </Link>

            <div className="login__container">
                <h1>Sign In</h1>
                <form>
                    <h5>E-mail</h5>
                    <input value={email} onChange={event => setEmail(event.target.value)} type="text"/>
                    <h5>Password</h5>
                    <input value={password} onChange={event => setPassword(event.target.value)} type="password"/>
                    <button onClick={login} className="login__signin">Sign In</button>
                </form>
                <p>
                    By signing in, you agree the terms and conditions.
                </p>
                <button onClick={register} className="login__register">Create Account</button>
            </div>
        </div>
    )
}

export default Login;
