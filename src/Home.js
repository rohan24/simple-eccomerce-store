import React from 'react';
import "./Home.css";
import Product from "./Product";
import Banner from './images/tro-banner.jpg';
import Grid from '@material-ui/core/Grid';

function Home() {
    return (
        <div className="home">
           <img className="home__image" src={Banner}/>
           <h1 className="title__products">Popular Products</h1>
           <Grid container justify="center" className="home__row">
                <Grid Item>
                <Product 
                    id="123"
                    title="Nike shoes"
                    price={90.99}
                    rating={5}
                    image="https://static.nike.com/a/images/c_limit,w_318,f_auto/t_product_v1/f20582bd-ce2f-44e6-8877-648a2d05e11e/air-max-270-react-womens-shoe-trW1vK.jpg"
                    />
                </Grid>
                <Grid Item>
                <Product 
                    id="456"
                    title="Men watch Lighter Watches quartz Military USB Charging"
                    price={100.00}
                    rating={4}
                    image="//cdn.shopify.com/s/files/1/1197/2796/products/Men-watch-Lighter-Watches-quartz-Military-USB-Charging-F665-Hot-sports-Casual-Wristwatches-Windproof-Cigarette-Lighter_5a9faed4-17e9-4cc9-9feb-fa85991a9558_large.jpg?v=1595304390"
                    />
                </Grid>
                <Grid Item>
                <Product 
                    id="789"
                    title="women's short spring and autumn clothing 2019 new"
                    price={90.99}
                    rating={4}
                    image="//cdn.shopify.com/s/files/1/1197/2796/products/12498932699_920017939_large.jpg?v=1593339209"
                    />
                </Grid>
                <Grid Item>
                <Product 
                    id="981"
                    title="Mid-length woolen coat winter new style woolen"
                    price={40.99}
                    rating={5}
                    image="//cdn.shopify.com/s/files/1/1197/2796/products/11890477044_1974332011_large.jpg?v=1587274928"
                    />
                </Grid>
           </Grid>
            <h1 className="title__products">Recent Products</h1>
            <Grid container justify="center" className="home__row">
            <Grid Item>
                <Product 
                    id="224"
                    title="Mosaic fashion coat collar male models fall men's"
                    price={80.99}
                    rating={5}
                    image="//cdn.shopify.com/s/files/1/1197/2796/products/9347478230_287164840_large.jpg?v=1575947348"
                    />
            </Grid>
                <Product 
                    id="167"
                    title="100 % Genuine Leather Wallet for Men"
                    price={13.99}
                    rating={4}
                    image="//cdn.shopify.com/s/files/1/1197/2796/products/IMG_20200224_144722_large.jpg?v=1594996194"
                    />
            <Grid Item>
                <Product 
                    id="123"
                    title="Palalula Men's Music Chester Bennington Linkin Park"
                    price={20.99}
                    rating={5}
                    image="//cdn.shopify.com/s/files/1/1197/2796/products/Capture_1eff3e09-c49b-4c36-b3ce-014e5a9ee080_large.jpg?v=1595480642"
                    />
            </Grid>
            <Grid Item>
                <Product 
                    id="894"
                    title="Royal Son Clear Stylish Unisex Sunglasses"
                    price={20.99}
                    rating={4}
                    image="//cdn.shopify.com/s/files/1/1197/2796/products/31sxGJ3I7EL._UL1500_large.jpg?v=1596698756"
                    />
            </Grid>
            </Grid>
        </div>
    )
}

export default Home;
