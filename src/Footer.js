import React from 'react';
import './Footer.css';
import { Link } from "react-router-dom";
import FacebookIcon from "@material-ui/icons/Facebook";
import InstagramIcon from "@material-ui/icons/Instagram";
import LinkedInIcon from "@material-ui/icons/LinkedIn";


function Footer() {
    return (
        <div className="footer">
            <div className="footer__copy">
                <span>&copy; 2020 RONSTORE</span>
            </div>
            <div className="footer__icons">
                <Link to="/"><FacebookIcon className="icon" /></Link>
                <Link to="/"><InstagramIcon className="icon" /></Link>
                <Link to="/"><LinkedInIcon className="icon" /></Link>
            </div>
        </div>
    )
}

export default Footer;
