import React from 'react';
import './Header.css';
import { Link } from "react-router-dom";
import SearchIcon from "@material-ui/icons/Search";
import ShoppingBasketIcon from "@material-ui/icons/ShoppingBasket";
import { useStateValue } from './StateProvider';
import { auth } from './firebase';
import logo from './images/mylogo.PNG';


function Header() {
    const [{ basket, user }] = useStateValue();

    const login = () => {
        if (user) {
            auth.signOut();
        }
    }
    // console.log(basket);
    return (
        <nav className="header">

            {/* logo */}

            <Link to="/">
           <img className="header__logo" src={logo} alt="logo"/>
           </Link>

           {/* Search Box */}
           <div className="header__search">
            <input type="text" class="header__searchInput" />
            <SearchIcon className="header__searchIcon"/>
            </div>

            {/* 3 links */}
            <div className="header__nav">
                <Link to={!user && "/login"} className="header__link">
                    <div onClick ={login} className="header__option">
                        <span className="header__optionLineOne">Hello {user?.email} </span>
                        <span className="header__optionLineTwo">{user ? 'Sign Out': 'Sign in'}</span>
                    </div>
                </Link>
                <Link to="/checkout" className="header__link">
                    <div className="header__optionBasket">
                        <ShoppingBasketIcon />
    <span className="header__optionLineTwo header_basketCount">{basket?.length}</span>
                    </div>
                </Link>
           </div>
        </nav>
    )
}

export default Header
