import firebase from "firebase";

const firebaseApp= firebase.initializeApp({
  apiKey: "AIzaSyA0F5yr8Vx2oquYLtSHzPdSHN9lSldR07k",
  authDomain: "rohanstore-4a54f.firebaseapp.com",
  databaseURL: "https://rohanstore-4a54f.firebaseio.com",
  projectId: "rohanstore-4a54f",
  storageBucket: "rohanstore-4a54f.appspot.com",
  messagingSenderId: "799518766055",
  appId: "1:799518766055:web:2444af9914936654608342",
  measurementId: "G-4SC56PWW5B"
});

const db= firebaseApp.firestore();
const auth = firebase.auth();

export { db, auth };
