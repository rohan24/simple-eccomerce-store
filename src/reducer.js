export const initialState = {
    basket: [
        // {
        //     id: "123",
        //     title: "Nike shoes",
        //     price: 90.99,
        //     rating: 5,
        //     image: "https://static.nike.com/a/images/c_limit,w_318,f_auto/t_product_v1/f20582bd-ce2f-44e6-8877-648a2d05e11e/air-max-270-react-womens-shoe-trW1vK.jpg" ,
        // },
        // {
        //     id: "123",
        //     title: "Nike shoes",
        //     price: 90.99,
        //     rating: 5,
        //     image: "https://static.nike.com/a/images/c_limit,w_318,f_auto/t_product_v1/f20582bd-ce2f-44e6-8877-648a2d05e11e/air-max-270-react-womens-shoe-trW1vK.jpg" ,
        // },
],
    user: null,
};

export const getBasketTotal = (basket) =>
basket?.reduce ((amount, item) => item.price + amount, 0);

const reducer = (state, action) => {
    console.log(action);
   switch(action.type) {
       case 'SET_USER':
           return{
               ...state,
               user: action.user,
           };
       case 'ADD_TO_BASKET':
       //logic for adding items to basket
       return{
           ...state,
           basket: [...state.basket, action.item],
       };
       case 'REMOVE_FROM_BASKET':
        // Remove

        //cloned the basket
        let newBasket = [...state.basket];

        const index = state.basket.findIndex((basketItem) => basketItem.id === action.id);
        
        if (index >= 0) {
            //item exists
            newBasket.splice(index, 1);
        } else {
            console.warn('Cant remove product'
            );
        }
        
        return { ...state, basket: newBasket, };
        default:
        return state;
   }
};

export default reducer;